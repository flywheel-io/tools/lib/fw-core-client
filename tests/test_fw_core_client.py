"""Test CoreClient input validation and initialization."""
# pylint: disable=redefined-outer-name
import functools
import io
import json

import pydantic
import pytest
from fw_http_client import errors

import fw_core_client

pytest_plugins = "fw_http_testserver"

# CoreClient requires name/version - use test/1.0 throughout tests
client_info = dict(client_name="test", client_version="1.0")
CoreClient = functools.partial(fw_core_client.CoreClient, **client_info)


def test_api_key_with_prefix():
    client = CoreClient(api_key="scitran-user host:key")
    assert client.baseurl == "https://host"
    assert client.headers["Authorization"] == "scitran-user host:key"


def test_api_key_with_embedded_host():
    client = CoreClient(api_key="host:key")
    assert client.baseurl == "https://host"
    assert client.headers["Authorization"] == "scitran-user host:key"


def test_api_key_with_embedded_host_and_custom_port():
    client = CoreClient(api_key="host:8443:key")
    assert client.baseurl == "https://host:8443"
    assert client.headers["Authorization"] == "scitran-user host:8443:key"


def test_api_key_with_embedded_host_and_custom_port_using_http():
    client = CoreClient(api_key="http://host:8080:key")
    assert client.baseurl == "http://host:8080"
    assert client.headers["Authorization"] == "scitran-user http://host:8080:key"


def test_api_key_without_embedded_host():
    client = CoreClient(api_key="key", url="host/")
    assert client.baseurl == "https://host"
    assert client.headers["Authorization"] == "scitran-user key"


def test_no_api_key_or_url_raises():
    error = r".*api_key or url required.*"
    with pytest.raises(pydantic.ValidationError, match=error):
        CoreClient()


def test_api_key_without_embedded_host_raises():
    error = r".*api_key with url expected.*"
    with pytest.raises(pydantic.ValidationError, match=error):
        CoreClient(api_key="key")


def test_api_key_without_embedded_host_and_url():
    client = CoreClient(api_key="key", url="https://host")
    assert client.baseurl == "https://host"
    assert client.headers["Authorization"] == "scitran-user key"


@pytest.fixture
def core(http_testserver):
    return http_testserver


def test_drone_secret(core):
    core.add_response("/api/devices/self", {"key": "key"})
    client = CoreClient(
        url=core.url,
        drone_secret="secret",
        device_label="name",
    )
    assert client.headers["Authorization"] == "scitran-user key"
    assert core.request_log == ["GET /api/devices/self"]
    get_devices_self = core.requests[0]
    assert get_devices_self.headers["X-Scitran-Auth"] == "secret"
    assert get_devices_self.headers["X-Scitran-Method"] == "test"
    assert get_devices_self.headers["X-Scitran-Name"] == "name"


def test_drone_secret_without_device_label_raises():
    with pytest.raises(pydantic.ValidationError, match=r".*device_label required.*"):
        CoreClient(url="https://host", drone_secret="secret")


def test_http_settings():
    client = CoreClient(api_key="host:key", params={"k": "v"})
    assert client.params["k"] == "v"


def test_auth_missing_raises():
    error = r".*api_key or drone_secret required.*"
    with pytest.raises(pydantic.ValidationError, match=error):
        CoreClient(url="https://host/api")


def test_auth_proxy(core):
    client = CoreClient(url=core.url, auth_proxy=True)
    assert "Authorization" not in client.headers

    core.add_response("/api/test")
    client.get("/test", auth="token")
    assert core.request_log == ["GET /api/test"]
    assert core.requests[0].headers["Authorization"] == "token"


def test_auth_proxy_raises_if_credential_specified(core):
    error = r".*api_key and drone_secret not allowed with auth_proxy.*"
    with pytest.raises(pydantic.ValidationError, match=error):
        CoreClient(url=core.url, api_key="host:key", auth_proxy=True)
    with pytest.raises(pydantic.ValidationError, match=error):
        CoreClient(url=core.url, drone_secret="secret", auth_proxy=True)


def test_alternative_service_url(core):
    client = CoreClient(
        url=core.url,
        api_key="key",
        io_proxy_url=f"{core.url}/test/",
        xfer_url=f"{core.url}/test",
    )
    core.add_response("/test/io-proxy/test")
    core.add_response("/test/xfer/test")
    client.get("/io-proxy/test")
    client.get("/xfer/test")
    assert core.request_log == ["GET /test/io-proxy/test", "GET /test/xfer/test"]


@pytest.fixture
def client(core) -> fw_core_client.CoreClient:
    """Return CoreClient configured for the HTTP test server."""
    return CoreClient(
        api_key=f"{core.url}:key",
        retry_status_forcelist=[],
        retry_total=0,
    )


def test_alternative_api_prefix(client, core):
    core.add_response("/io-proxy/test")
    client.get("/io-proxy/test")
    assert core.request_log == ["GET /io-proxy/test"]


def test_check_feature(client, core):
    core.add_response("/api/config", {"features": {"foo": True}})

    assert client.check_feature("foo") is True
    assert client.check_feature("bar") is False
    assert core.request_log == ["GET /api/config"]


def test_check_version(client, core):
    core.add_response("/api/version", {"release": "1.2.3"})

    assert client.check_version("1.2.2") is True
    assert client.check_version("1.2.3") is True
    assert client.check_version("1.2.4") is False
    assert core.request_log == ["GET /api/version"]


def test_check_version_none(client, core):
    core.add_response("/api/version", {"release": None})
    assert client.check_version("1.2.3") is True


def test_device_status(client, core):
    status = {"is_device": True, "origin": {"id": "123"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/123", {"foo": "bar"})

    assert client.auth_status == {
        "is_device": True,
        "origin": {"id": "123"},
        "info": {"foo": "bar"},
    }
    assert core.request_log == ["GET /api/auth/status", "GET /api/devices/123"]


def test_user_status(client, core):
    core.add_response("/api/auth/status", {"is_device": False})
    core.add_response("/api/users/self", {"bar": "baz"})

    assert client.auth_status == {"is_device": False, "info": {"bar": "baz"}}
    assert core.request_log == ["GET /api/auth/status", "GET /api/users/self"]


def test_upload(tmp_path, client, core):
    core.add_response("/api/config", {})
    core.add_response("/api/upload/label", {"id": "123"}, method="POST")
    tmp_file = tmp_path / "test.txt"
    tmp_file.write_text("text")
    metadata = {
        "group": {"_id": "group"},
        "project": {"label": "project"},
        "file": {"name": "test.txt", "type": "text"},
    }

    assert client.upload(str(tmp_file), metadata) == {"id": "123"}
    assert core.request_log == ["GET /api/config", "POST /api/upload/label"]
    upload_request = core.requests[1]
    assert upload_request.files["test.txt"] == b"text"
    assert json.loads(upload_request.files["metadata"]) == {
        "group": {"_id": "group"},
        "project": {
            "label": "project",
            "files": [{"name": "test.txt", "type": "text"}],
        },
    }


def test_upload_metadata_with_file_info(tmp_path, client, core):
    core.add_response("/api/config", {})
    core.add_response("/api/upload/reaper", {"id": "123"}, method="POST")
    tmp_file = tmp_path / "test.txt"
    tmp_file.write_text("text")
    metadata = {
        "subject": {"label": "S001"},
        "file": {"name": "test.txt", "type": "text"},
    }

    response = client.upload(str(tmp_file), metadata, method="reaper")
    assert response == {"id": "123"}
    assert core.request_log == ["GET /api/config", "POST /api/upload/reaper"]
    upload_request = core.requests[1]
    assert upload_request.files["test.txt"] == b"text"
    assert json.loads(upload_request.files["metadata"]) == {
        "session": {
            "subject": {
                "label": "S001",
                "files": [{"name": "test.txt", "type": "text"}],
            }
        }
    }


def test_upload_metadata_fill(tmp_path, client, core):
    core.add_response("/api/config", {})
    core.add_response("/api/upload/reaper", {"id": "123"}, method="POST")
    tmp_file = tmp_path / "test.txt"
    tmp_file.write_text("text")
    metadata = {"subject": {"label": "S001"}}

    response = client.upload(str(tmp_file), metadata, method="reaper", fill=True)
    assert response == {"id": "123"}
    assert core.request_log == ["GET /api/config", "POST /api/upload/reaper"]
    upload_request = core.requests[1]
    assert upload_request.files["test.txt"] == b"text"
    assert json.loads(upload_request.files["metadata"]) == {
        "group": {"_id": ""},
        "project": {"label": ""},
        "session": {
            "uid": "",
            "subject": {"label": "S001"},
        },
        "acquisition": {"uid": "", "files": [{"name": "test.txt"}]},
    }


def test_upload_signed_url(tmp_path, client, core):
    def callback():
        # upload start - ticket creation
        if core.request.args["ticket"] == "":
            signed_url = f"{core.url}/storage/signed-url"
            data = dict(ticket="ticket", urls={"meta-name": signed_url})
            return core.jsonify(data), 200, {}
        # upload finish
        return core.jsonify(id="123"), 200, {}

    core.add_response("/api/config", {"signed_url": True})
    core.add_callback("/api/upload/reaper", callback, methods=["POST"])
    core.add_response("/storage/signed-url", method="PUT")
    tmp_file = tmp_path / "test.txt"
    tmp_file.write_text("text")
    metadata = {
        "group": {"_id": "group"},
        "project": {"label": "project", "files": [{"name": "meta-name"}]},
    }

    assert client.upload(str(tmp_file), metadata, method="reaper") == {"id": "123"}
    assert core.request_log == [
        "GET /api/config",
        "POST /api/upload/reaper",
        "PUT /storage/signed-url",
        "POST /api/upload/reaper",
    ]
    upload_start, upload_data, upload_finish = core.requests[1:]
    assert upload_start.params == {"ticket": ""}
    assert upload_start.json == {
        "filenames": ["meta-name"],
        "metadata": {
            "group": {"_id": "group"},
            "project": {"label": "project", "files": [{"name": "meta-name"}]},
        },
    }
    assert upload_data.body == b"text"
    assert upload_finish.params == {"ticket": "ticket"}


def test_store_file_using_signed_url(client, core):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/123", {"foo": "bar"})
    core.add_response("/signed-url", method="PUT")
    file_resp = {"storage_file_id": "file_id", "upload_url": f"{core.url}/signed-url"}
    core.add_response("/api/storage/files", file_resp, method="POST")

    assert client.store_file("project_id", io.BytesIO()) == "file_id"


def test_store_file_fallback_to_direct_upload(client, core):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/123", {"foo": "bar"})

    def callback():
        if core.request.args.get("signed_url"):
            return {}, 409
        return {"storage_file_id": "file_id"}, 200

    core.add_callback("/api/storage/files", callback, methods=["POST"])

    f_id = client.store_file("project_id", io.BytesIO(), content_encoding="gzip")
    assert f_id == "file_id"
    assert core.requests[-2].headers["Content-Encoding"] == "gzip"


def test_store_file_raises_if_not_device(client, core):
    core.add_response("/api/auth/status", {"is_device": False})
    core.add_response("/api/users/self", {"foo": "bar"})

    with pytest.raises(AssertionError, match="Device authentication required"):
        client.store_file("project_id", io.BytesIO())


def test_store_file_raises_on_http_error(client, core):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/123", {"foo": "bar"})
    core.add_response("/api/storage/files", method="POST", status=400)

    with pytest.raises(errors.HTTPError):
        client.store_file("project_id", io.BytesIO())


def test_store_file_deletes_file_from_core_on_signed_url_error(client, core):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/123", {"foo": "bar"})
    core.add_response("/signed-url", method="PUT", status=502)
    file_resp = {"storage_file_id": "file_id", "upload_url": f"{core.url}/signed-url"}
    core.add_response("/api/storage/files", file_resp, method="POST")
    core.add_response("/api/storage/files/file_id", {}, method="DELETE")

    with pytest.raises(errors.HTTPError):
        client.store_file("project_id", io.BytesIO())

    assert "DELETE /api/storage/files/file_id" in core.request_log
