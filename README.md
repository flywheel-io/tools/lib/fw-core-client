# fw-core-client

**fw-core-client is an archived library.**<br/>
**It's successor is [fw-client](https://gitlab.com/flywheel-io/tools/lib/fw-client),**<br/>
**renamed to avoid confusion when accessing microservices besides core-api internally.**

Flywheel Core HTTP API client.

## Installation

Add as a `poetry` dependency to your project:

```bash
poetry add fw-core-client
```

## Usage

```python
from fw_core_client import CoreClient

core_client = CoreClient(
    api_key="site.flywheel.io:699uxdIh2wmqtdDyLJ",
    client_name="my-app",
    client_version="1.0",
)
projects = core_client.get("/projects")
project_labels = [proj.label for proj in projects]
```

## Development

Install the project using `poetry` and enable `pre-commit`:

```bash
poetry install
pre-commit install
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
